//
//  ClientTests.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 02/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Client.h"
#import "FakeNetworkSession.h"
#import "Car.h"

@interface ClientTests : XCTestCase

@property (nonatomic, strong, readwrite) Client *client;
@property (nonatomic, strong, readwrite) FakeNetworkSession *fakeNetworkSession;

@end

@implementation ClientTests

- (void)setUp
{
  [super setUp];
  self.fakeNetworkSession = [[FakeNetworkSession alloc] init];
  self.client = [[Client alloc] initWithNetworkSession:self.fakeNetworkSession];
}

- (void)tearDown
{
  self.client = nil;
  self.fakeNetworkSession = nil;
  [super tearDown];
}

- (void)testAllCars
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"client returned"];

  NSString *identifier = @"WMWSW31030T222518";
  NSString *modelIdentifier = @"mini";
  NSString *modelName = @"MINI";
  NSString *name = @"Vanessa";
  NSString *make = @"BMW";
  NSString *group = @"MINI";
  NSString *color = @"midnight_black";
  NSString *series = @"MINI";
  NSString *fuelType = @"D";
  NSNumber *fuelLevel = @(0.7);
  NSString *transmission = @"M";
  NSString *licensePlate = @"M-VO0259";
  NSNumber *latitude = @(48.134557);
  NSNumber *longitude = @(11.576921);
  NSString *innerCleanliness = @"REGULAR";
  NSString *carImageUrl = @"https://de.drive-now.com/static/drivenow/img/cars/mini.png";

  NSArray *responseObject =
  @[@{
      @"id": identifier,
      @"modelIdentifier": modelIdentifier,
      @"modelName": modelName,
      @"name": name,
      @"make": make,
      @"group": group,
      @"color": color,
      @"series": series,
      @"fuelType": fuelType,
      @"fuelLevel": fuelLevel,
      @"transmission": transmission,
      @"licensePlate": licensePlate,
      @"latitude": latitude,
      @"longitude": longitude,
      @"innerCleanliness": innerCleanliness,
      @"carImageUrl": carImageUrl
      }];
  self.fakeNetworkSession.responseObject = responseObject;

  [self.client
   allCars:^(NSArray *cars) {
     XCTAssertNotNil(cars);
     XCTAssertEqual(1, [cars count]);

     Car *car = cars[0];
     XCTAssertEqualObjects(identifier, car.identifier);
     XCTAssertEqualObjects(modelIdentifier, car.modelIdentifier);
     XCTAssertEqualObjects(modelName, car.modelName);
     XCTAssertEqualObjects(name, car.name);
     XCTAssertEqualObjects(make, car.make);
     XCTAssertEqualObjects(group, car.group);
     XCTAssertEqualObjects(color, car.color);
     XCTAssertEqualObjects(series, car.series);
     XCTAssertEqualObjects(fuelType, car.fuelType);
     XCTAssertEqual([fuelLevel floatValue], car.fuelLevel);
     XCTAssertEqualObjects(transmission, car.transmission);
     XCTAssertEqualObjects(licensePlate, car.licensePlate);
     XCTAssertEqual([latitude doubleValue], car.latitude);
     XCTAssertEqual([longitude doubleValue], car.longitude);
     XCTAssertEqualObjects(innerCleanliness, car.innerCleanliness);
     XCTAssertEqualObjects([NSURL URLWithString:carImageUrl], car.carImageUrl);

     [expectation fulfill];
   }
   failure:^(NSError *error) {
     XCTFail(@"no error expected");
     [expectation fulfill];
   }];

  [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
    XCTAssertNil(error);
  }];
}

- (void)testBadResponse
{
  XCTestExpectation *expectation = [self expectationWithDescription:@"client returned"];

  self.fakeNetworkSession.responseObject = @{};
  [self.client
   allCars:^(NSArray *cars) {
     XCTFail(@"error expected");
     [expectation fulfill];
   }
   failure:^(NSError *error) {
     XCTAssertNotNil(error);
     [expectation fulfill];
   }];

  [self waitForExpectationsWithTimeout:1.0 handler:^(NSError *error) {
    XCTAssertNil(error);
  }];
}

@end
