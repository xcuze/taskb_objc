//
//  CarTests.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Car.h"

@interface CarTests : XCTestCase

@property (nonatomic, strong, readwrite) NSString *jsonIdentifier;
@property (nonatomic, strong, readwrite) NSString *jsonModelIdentifier;
@property (nonatomic, strong, readwrite) NSString *jsonModelName;
@property (nonatomic, strong, readwrite) NSString *jsonName;
@property (nonatomic, strong, readwrite) NSString *jsonMake;
@property (nonatomic, strong, readwrite) NSString *jsonGroup;
@property (nonatomic, strong, readwrite) NSString *jsonColor;
@property (nonatomic, strong, readwrite) NSString *jsonSeries;
@property (nonatomic, strong, readwrite) NSString *jsonFuelType;
@property (nonatomic, strong, readwrite) NSNumber *jsonFuelLevel;
@property (nonatomic, strong, readwrite) NSString *jsonTransmission;
@property (nonatomic, strong, readwrite) NSString *jsonLicensePlate;
@property (nonatomic, strong, readwrite) NSNumber *jsonLatitude;
@property (nonatomic, strong, readwrite) NSNumber *jsonLongitude;
@property (nonatomic, strong, readwrite) NSString *jsonInnerCleanliness;
@property (nonatomic, strong, readwrite) NSString *jsonCarImageUrl;

@property (nonatomic, strong, readwrite) NSDictionary *json;

@end

@implementation CarTests

- (void)setUp
{
  self.jsonIdentifier = @"WMWSW31030T222518";
  self.jsonModelIdentifier = @"mini";
  self.jsonModelName = @"MINI";
  self.jsonName = @"Vanessa";
  self.jsonMake = @"BMW";
  self.jsonGroup = @"MINI";
  self.jsonColor = @"midnight_black";
  self.jsonSeries = @"MINI";
  self.jsonFuelType = @"D";
  self.jsonFuelLevel = @(0.7);
  self.jsonTransmission = @"M";
  self.jsonLicensePlate = @"M-VO0259";
  self.jsonLatitude = @(48.134557);
  self.jsonLongitude = @(11.576921);
  self.jsonInnerCleanliness = @"REGULAR";
  self.jsonCarImageUrl = @"https://de.drive-now.com/static/drivenow/img/cars/mini.png";

  self.json = @{
                @"id": self.jsonIdentifier,
                @"modelIdentifier": self.jsonModelIdentifier,
                @"modelName": self.jsonModelName,
                @"name": self.jsonName,
                @"make": self.jsonMake,
                @"group": self.jsonGroup,
                @"color": self.jsonColor,
                @"series": self.jsonSeries,
                @"fuelType": self.jsonFuelType,
                @"fuelLevel": self.jsonFuelLevel,
                @"transmission": self.jsonTransmission,
                @"licensePlate": self.jsonLicensePlate,
                @"latitude": self.jsonLatitude,
                @"longitude": self.jsonLongitude,
                @"innerCleanliness": self.jsonInnerCleanliness,
                @"carImageUrl": self.jsonCarImageUrl
                };
  [super setUp];
}

- (void)testSingleDeserialization
{
  Car *car = [Car carFromJson:self.json];

  XCTAssertNotNil(car);
  XCTAssertEqualObjects(self.jsonIdentifier, car.identifier);
  XCTAssertEqualObjects(self.jsonModelIdentifier, car.modelIdentifier);
  XCTAssertEqualObjects(self.jsonModelName, car.modelName);
  XCTAssertEqualObjects(self.jsonName, car.name);
  XCTAssertEqualObjects(self.jsonMake, car.make);
  XCTAssertEqualObjects(self.jsonGroup, car.group);
  XCTAssertEqualObjects(self.jsonColor, car.color);
  XCTAssertEqualObjects(self.jsonSeries, car.series);
  XCTAssertEqualObjects(self.jsonFuelType, car.fuelType);
  XCTAssertEqual([self.jsonFuelLevel floatValue], car.fuelLevel);
  XCTAssertEqualObjects(self.jsonTransmission, car.transmission);
  XCTAssertEqualObjects(self.jsonLicensePlate, car.licensePlate);
  XCTAssertEqual([self.jsonLatitude doubleValue], car.latitude);
  XCTAssertEqual([self.jsonLongitude doubleValue], car.longitude);
  XCTAssertEqualObjects(self.jsonInnerCleanliness, car.innerCleanliness);
  XCTAssertEqualObjects([NSURL URLWithString:self.jsonCarImageUrl], car.carImageUrl);
}

- (void)testMultiDeserialization
{
  NSArray *jsonArray = @[self.json, self.json, self.json];
  NSArray *cars = [Car carsFromJsonArray:jsonArray];

  XCTAssertNotNil(cars);
  XCTAssertEqual([jsonArray count], [cars count]);

  for (id object in cars) {
    XCTAssertTrue([object isKindOfClass:[Car class]]);
  }
}

@end
