//
//  FakeNetworkSession.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "FakeNetworkSession.h"

@implementation FakeNetworkSession

+ (id <NetworkSession>)sharedSession
{
  return [[FakeNetworkSession alloc] init];
}

- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url
                        completionHandler:(void (^)(NSData *data,
                                                    NSURLResponse *response,
                                                    NSError *error))completionHandler
{
  NSError *error;

  if (nil != self.responseObject) {
    NSData *data = [NSJSONSerialization dataWithJSONObject:self.responseObject options:0 error:&error];
    completionHandler(data, nil, error);
  } else {
    completionHandler(nil, nil, [NSError errorWithDomain:@"taskb" code:0 userInfo:nil]);
  }

  return nil;
}

@end
