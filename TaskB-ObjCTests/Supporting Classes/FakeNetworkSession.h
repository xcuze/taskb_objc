//
//  FakeNetworkSession.h
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkSession.h"

@interface FakeNetworkSession : NSObject <NetworkSession>
@property (nonatomic, strong, readwrite) id responseObject;
@end
