//
//  MainViewControllerTests.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <XCTest/XCTest.h>

#import "MainViewController.h"

@interface MainViewController (PrivateProperties)
@property (nonatomic, strong, readonly) UITableView *tableView;
@property (nonatomic, strong, readonly) MKMapView *mapView;

@end

@interface MainViewControllerTests : XCTestCase

@property (nonatomic, strong, readwrite) MainViewController *mainViewController;

@end

@implementation MainViewControllerTests

- (void)setUp
{
  [super setUp];
  self.mainViewController = [[MainViewController alloc] init];
  [self.mainViewController performSelectorOnMainThread:@selector(loadView) withObject:nil waitUntilDone:TRUE];
}

- (void)tearDown
{
  self.mainViewController = nil;
  [super tearDown];
}

- (void)testViewHierarchy
{
  XCTAssertNotNil(self.mainViewController.view);
  XCTAssertNotNil(self.mainViewController.tableView);
  XCTAssertTrue([self.mainViewController.view.subviews containsObject:self.mainViewController.tableView]);
  XCTAssertNotNil(self.mainViewController.mapView);
  XCTAssertTrue([self.mainViewController.view.subviews containsObject:self.mainViewController.mapView]);
}

- (void)testNavigationItem
{
  XCTAssertNotNil(self.mainViewController.navigationItem.rightBarButtonItem);
  XCTAssertNil(self.mainViewController.navigationItem.leftBarButtonItem);
  XCTAssertEqualObjects(self.mainViewController.title, @"Task B");
}

- (void)testInitialVisibility
{
  XCTAssertFalse(self.mainViewController.tableView.hidden);
  XCTAssertTrue(self.mainViewController.mapView.hidden);
}

- (void)testToggle
{
  UIBarButtonItem *toggle = self.mainViewController.navigationItem.rightBarButtonItem;
  _Pragma("clang diagnostic push")
  _Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"")
  [toggle.target performSelectorOnMainThread:toggle.action withObject:nil waitUntilDone:TRUE];
  _Pragma("clang diagnostic pop")

  XCTAssertTrue(self.mainViewController.tableView.hidden);
  XCTAssertFalse(self.mainViewController.mapView.hidden);
}

@end
