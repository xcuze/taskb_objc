//
//  CarsDataSource.h
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class CarsDataSource;

@protocol CarsDataSourceDelegate <NSObject>

- (void)dataSourceDidFinishLoading:(CarsDataSource *)dataSource;
- (void)dataSource:(CarsDataSource *)dataSource didFailToLoadWithError:(NSError *)error;

@end

@interface CarsDataSource : NSObject <UITableViewDataSource>

@property (nonatomic, weak, readwrite) id <CarsDataSourceDelegate> delegate;
@property (nonatomic, strong, readonly) NSArray *mapAnnotations;

- (void)load;

- (void)configureTableView:(UITableView *)tableView;

@end
