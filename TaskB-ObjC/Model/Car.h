//
//  Car.h
//  TaskB-ObjC
//
//  Created by Florian Krüger on 02/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Car : NSObject

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *modelIdentifier;
@property (nonatomic, strong, readonly) NSString *modelName;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *make;
@property (nonatomic, strong, readonly) NSString *group;
@property (nonatomic, strong, readonly) NSString *color;
@property (nonatomic, strong, readonly) NSString *series;
@property (nonatomic, strong, readonly) NSString *fuelType;
@property (nonatomic, assign, readonly) float fuelLevel;
@property (nonatomic, strong, readonly) NSString *transmission;
@property (nonatomic, strong, readonly) NSString *licensePlate;
@property (nonatomic, assign, readonly) double latitude;
@property (nonatomic, assign, readonly) double longitude;
@property (nonatomic, strong, readonly) NSString *innerCleanliness;
@property (nonatomic, strong, readonly) NSURL *carImageUrl;

+ (NSArray *)carsFromJsonArray:(NSArray *)jsonArray;
+ (Car *)carFromJson:(NSDictionary *)json;

- (CLLocationCoordinate2D)coordinate;

@end
