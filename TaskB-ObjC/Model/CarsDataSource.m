//
//  CarsDataSource.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "CarsDataSource.h"

#import "CarTableViewCell.h"
#import "Client.h"

@interface CarsDataSource ()

@property (nonatomic, strong, readwrite) NSString *cellIdentifier;
@property (nonatomic, strong, readwrite) NSArray *cars;
@property (nonatomic, strong, readwrite) NSArray *mapAnnotations;

@end

@implementation CarsDataSource

#pragma mark - Init

- (instancetype)init
{
  self = [super init];
  if (self) {
    _cars = [NSArray array];
    _cellIdentifier = @"taskb.cell.car";
  }
  return self;
}

#pragma mark - Loading

- (void)load
{
  __weak CarsDataSource *weakself = self;
  [[Client sharedClient]
   allCars:^(NSArray *cars) {
     weakself.cars = cars;
     [weakself clearAnnotations];
     dispatch_async(dispatch_get_main_queue(), ^{
       [weakself.delegate dataSourceDidFinishLoading:weakself];
     });
   }
   failure:^(NSError *error) {
     dispatch_async(dispatch_get_main_queue(), ^{
       [weakself.delegate dataSource:weakself didFailToLoadWithError:error];
     });
   }];
}

#pragma mark - Annotations

- (NSArray *)mapAnnotations
{
  if (nil == _mapAnnotations) {
    [self createAnnotations];
  }
  return _mapAnnotations;
}

- (void)createAnnotations
{
  NSMutableArray *annotations = [NSMutableArray arrayWithCapacity:[self.cars count]];
  for (Car *car in self.cars) {
    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
    pointAnnotation.coordinate = car.coordinate;
    pointAnnotation.title = car.name;
    [annotations addObject:pointAnnotation];
  }
  self.mapAnnotations = [NSArray arrayWithArray:annotations];
}

- (void)clearAnnotations
{
  _mapAnnotations = nil;
}

#pragma mark - TableView

- (void)configureTableView:(UITableView *)tableView
{
  [tableView registerClass:[CarTableViewCell class] forCellReuseIdentifier:self.cellIdentifier];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [_cars count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  CarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
  if ([self.cars count] > indexPath.row) {
    cell.car = self.cars[indexPath.row];
  } else {
    cell.car = nil;
  }
  return cell;
}

@end
