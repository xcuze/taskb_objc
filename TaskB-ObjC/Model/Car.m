//
//  Car.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 02/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "Car.h"

@interface Car ()

@property (nonatomic, strong, readwrite) NSString *identifier;
@property (nonatomic, strong, readwrite) NSString *modelIdentifier;
@property (nonatomic, strong, readwrite) NSString *modelName;
@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSString *make;
@property (nonatomic, strong, readwrite) NSString *group;
@property (nonatomic, strong, readwrite) NSString *color;
@property (nonatomic, strong, readwrite) NSString *series;
@property (nonatomic, strong, readwrite) NSString *fuelType;
@property (nonatomic, assign, readwrite) float fuelLevel;
@property (nonatomic, strong, readwrite) NSString *transmission;
@property (nonatomic, strong, readwrite) NSString *licensePlate;
@property (nonatomic, assign, readwrite) double latitude;
@property (nonatomic, assign, readwrite) double longitude;
@property (nonatomic, strong, readwrite) NSString *innerCleanliness;
@property (nonatomic, strong, readwrite) NSURL *carImageUrl;

@end

@implementation Car

#pragma mark - Factory

+ (NSArray *)carsFromJsonArray:(NSArray *)jsonArray
{
  NSMutableArray *cars = [NSMutableArray arrayWithCapacity:[jsonArray count]];
  for (NSDictionary *json in jsonArray) {
    Car *car = [self carFromJson:json];
    if (nil != car) {
      [cars addObject:car];
    }
  }
  return cars;
}

+ (Car *)carFromJson:(NSDictionary *)json
{
  Car *car = [[Car alloc] init];

  id identifier = json[@"id"];
  if (nil != identifier && [identifier isKindOfClass:[NSString class]]) {
    car.identifier = identifier;
  } else {
    return nil;
  }

  id modelIdentifier = json[@"modelIdentifier"];
  if (nil != modelIdentifier && [modelIdentifier isKindOfClass:[NSString class]]) {
    car.modelIdentifier = modelIdentifier;
  } else {
    return nil;
  }

  id modelName = json[@"modelName"];
  if (nil != modelName && [modelName isKindOfClass:[NSString class]]) {
    car.modelName = modelName;
  } else {
    return nil;
  }

  id name = json[@"name"];
  if (nil != name && [name isKindOfClass:[NSString class]]) {
    car.name = name;
  } else {
    return nil;
  }

  id make = json[@"make"];
  if (nil != make && [make isKindOfClass:[NSString class]]) {
    car.make = make;
  } else {
    return nil;
  }

  id group = json[@"group"];
  if (nil != group && [group isKindOfClass:[NSString class]]) {
    car.group = group;
  } else {
    return nil;
  }

  id color = json[@"color"];
  if (nil != color && [color isKindOfClass:[NSString class]]) {
    car.color = color;
  } else {
    return nil;
  }

  id series = json[@"series"];
  if (nil != series && [series isKindOfClass:[NSString class]]) {
    car.series = series;
  } else {
    return nil;
  }

  id fuelType = json[@"fuelType"];
  if (nil != fuelType && [fuelType isKindOfClass:[NSString class]]) {
    car.fuelType = fuelType;
  } else {
    return nil;
  }

  id fuelLevel = json[@"fuelLevel"];
  if (nil != fuelLevel && [fuelLevel isKindOfClass:[NSNumber class]]) {
    car.fuelLevel = [fuelLevel doubleValue];
  } else {
    return nil;
  }

  id transmission = json[@"transmission"];
  if (nil != transmission && [transmission isKindOfClass:[NSString class]]) {
    car.transmission = transmission;
  } else {
    return nil;
  }

  id licensePlate = json[@"licensePlate"];
  if (nil != licensePlate && [licensePlate isKindOfClass:[NSString class]]) {
    car.licensePlate = licensePlate;
  } else {
    return nil;
  }

  id latitude = json[@"latitude"];
  if (nil != latitude && [latitude isKindOfClass:[NSNumber class]]) {
    car.latitude = [latitude doubleValue];
  } else {
    return nil;
  }

  id longitude = json[@"longitude"];
  if (nil != longitude && [longitude isKindOfClass:[NSNumber class]]) {
    car.longitude = [longitude doubleValue];
  } else {
    return nil;
  }

  id innerCleanliness = json[@"innerCleanliness"];
  if (nil != innerCleanliness && [innerCleanliness isKindOfClass:[NSString class]]) {
    car.innerCleanliness = innerCleanliness;
  } else {
    return nil;
  }

  id carImageUrl = json[@"carImageUrl"];
  if (nil != carImageUrl && [carImageUrl isKindOfClass:[NSString class]]) {
    car.carImageUrl = [NSURL URLWithString:carImageUrl];
  } else {
    return nil;
  }

  return car;
}

- (CLLocationCoordinate2D)coordinate
{
  return CLLocationCoordinate2DMake(self.latitude, self.longitude);
}

@end
