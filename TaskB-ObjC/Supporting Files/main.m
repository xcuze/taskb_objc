//
//  main.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 02/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
