//
//  MainViewController.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 02/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "MainViewController.h"

#import <MapKit/MapKit.h>
#import "AutoLayoutKit.h"

#import "CarsDataSource.h"

@interface MainViewController () <UITableViewDelegate, CarsDataSourceDelegate>

@property (nonatomic, strong, readwrite) UITableView *tableView;
@property (nonatomic, strong, readwrite) MKMapView *mapView;
@property (nonatomic, strong, readwrite) CarsDataSource *dataSource;

@property (nonatomic, strong, readwrite) UIBarButtonItem *toggleButton;

@end

@implementation MainViewController

- (instancetype)init
{
  self = [super initWithNibName:nil bundle:nil];
  if (self) {
    self.title = @"Task B";
    self.dataSource = [[CarsDataSource alloc] init];
    self.dataSource.delegate = self;

    self.toggleButton = [[UIBarButtonItem alloc] initWithTitle:@"Karte" style:UIBarButtonItemStylePlain target:self action:@selector(toggle)];
    self.navigationItem.rightBarButtonItem = self.toggleButton;
  }
  return self;
}

- (void)loadView
{
  [super loadView];
  [self setupSubviews];
  [self setupLayout];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.dataSource configureTableView:self.tableView];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [self.dataSource load];
}

#pragma mark - UITableViewDelegate


#pragma mark - CarsDataSourceDelegate

- (void)dataSourceDidFinishLoading:(CarsDataSource *)dataSource
{
  [self.tableView reloadData];
  [self.mapView removeAnnotations:self.mapView.annotations];
  [self.mapView addAnnotations:self.dataSource.mapAnnotations];
  [self.mapView showAnnotations:self.dataSource.mapAnnotations animated:true];
}

- (void)dataSource:(CarsDataSource *)dataSource didFailToLoadWithError:(NSError *)error
{
  [[[UIAlertView alloc] initWithTitle:@"Fehler"
                              message:@"Die Liste der Fahrzeuge konnte nicht abgerufen werden"
                             delegate:nil
                    cancelButtonTitle:@"OK"
                    otherButtonTitles: nil] show];
}

#pragma mark - Target

- (void)toggle
{
  self.toggleButton.enabled = false;
  UIViewAnimationOptions options = UIViewAnimationOptionShowHideTransitionViews | UIViewAnimationOptionTransitionCrossDissolve;

  if (self.mapView.hidden) {
    [UIView transitionFromView:self.tableView toView:self.mapView duration:.25 options:options completion:^(BOOL finished) {
      self.toggleButton.title = @"Liste";
      self.toggleButton.enabled = true;
    }];
  } else {
    [UIView transitionFromView:self.mapView toView:self.tableView duration:.25 options:options completion:^(BOOL finished) {
      self.toggleButton.title = @"Karte";
      self.toggleButton.enabled = true;
    }];
  }
}

#pragma mark - Setup (Private)

- (void)setupSubviews
{
  self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
  self.tableView.delegate = self;
  self.tableView.dataSource = self.dataSource;
  [self.view addSubview:self.tableView];

  self.mapView = [[MKMapView alloc] initWithFrame:CGRectZero];
  self.mapView.hidden = true;
  [self.view addSubview:self.mapView];
}

- (void)setupLayout
{
  [ALKConstraints layout:self.tableView do:^(ALKConstraints *c) {
    [c alignAllEdgesTo:self.view];
  }];

  [ALKConstraints layout:self.mapView do:^(ALKConstraints *c) {
    [c alignAllEdgesTo:self.view];
  }];
}

@end
