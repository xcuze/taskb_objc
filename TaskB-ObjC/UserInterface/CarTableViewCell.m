//
//  CarTableViewCell.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "CarTableViewCell.h"

@implementation CarTableViewCell

- (void)setCar:(Car *)car
{
  _car = car;
  self.textLabel.text = _car.name;
}

- (void)prepareForReuse
{
  self.textLabel.text = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
