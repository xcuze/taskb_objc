//
//  CarTableViewCell.h
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Car.h"

@interface CarTableViewCell : UITableViewCell

@property (nonatomic, strong, readwrite) Car *car;

@end
