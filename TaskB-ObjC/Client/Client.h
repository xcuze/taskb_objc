//
//  Client.h
//  TaskB-ObjC
//
//  Created by Florian Krüger on 02/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkSession.h"

@interface Client : NSObject

@property (nonatomic, strong, readonly) id <NetworkSession> networkSession;

+ (Client *)sharedClient;

- (instancetype)initWithNetworkSession:(id <NetworkSession>)networkSession;

- (void)allCars:(void (^)(NSArray *cars))success
        failure:(void (^)(NSError *error))failure;

@end
