//
//  NetworkSessionImplementation.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 04/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "NetworkSessionImplementation.h"

@interface NetworkSessionImplementation ()

@property (nonatomic, strong, readwrite) NSURLSession *urlSession;

@end

@implementation NetworkSessionImplementation

+ (id <NetworkSession>)sharedSession
{
  static NetworkSessionImplementation *sharedSession = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedSession = [[self alloc] init];
  });
  return sharedSession;
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    _urlSession = [NSURLSession sharedSession];
  }
  return self;
}

- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url
                        completionHandler:(void (^)(NSData *data,
                                                    NSURLResponse *response,
                                                    NSError *error))completionHandler
{
  return [self.urlSession dataTaskWithURL:url completionHandler:completionHandler];
}

@end
