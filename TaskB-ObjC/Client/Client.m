//
//  Client.m
//  TaskB-ObjC
//
//  Created by Florian Krüger on 02/05/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "Client.h"
#import "NetworkSessionImplementation.h"
#import "Car.h"

@interface Client ()

@property (nonatomic, strong, readwrite) id <NetworkSession> networkSession;

@end

@implementation Client

#pragma mark - Singleton

+ (Client *)sharedClient
{
  static Client *sharedClient = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedClient = [[self alloc] initWithNetworkSession:[NetworkSessionImplementation sharedSession]];
  });
  return sharedClient;
}

#pragma mark - Init

- (instancetype)initWithNetworkSession:(id <NetworkSession>)networkSession
{
  self = [super init];
  if (self) {
    _networkSession = networkSession;
  }
  return self;
}

#pragma mark - Public API

- (void)allCars:(void (^)(NSArray *cars))success
        failure:(void (^)(NSError *error))failure
{
  NSURL *url = [NSURL URLWithString:@"http://www.codetalk.de/cars.json"];
  [self GET:url
    success:^(id responseObject) {
      if (nil != responseObject && [responseObject isKindOfClass:[NSArray class]]) {
        NSArray *cars = [Car carsFromJsonArray:responseObject];
        success(cars);
      } else {
        failure([NSError errorWithDomain:@"taskb" code:1 userInfo:@{@"message":@"invalid response"}]);
      }
    }
    failure:^(NSError *error) {
      failure(error);
    }];
}

#pragma mark - Network Communication

- (void)GET:(NSURL *)url success:(void (^)(id responseObject))success failure:(void (^)(NSError *))failure
{
  __weak Client *weakself = self;

  NSURLSessionDataTask *task =
  [self.networkSession
   dataTaskWithURL:url
   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
     if (nil != data) {
       [weakself parseJSON:data success:success failure:failure];
     } else {
       failure(error);
     }
   }];

  [task resume];
}

#pragma mark - JSON Parsing

- (void)parseJSON:(NSData *)jsonData success:(void (^)(id responseObject))success failure:(void (^)(NSError *))failure
{
  NSError *error;
  id responseObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];

  if (nil == error && nil != responseObject) {
    success(responseObject);
  } else {
    failure(error);
  }
}

@end
