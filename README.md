# Task B

Exemplarische Implementierung eines JSON/REST HTTP Clients und der Anzeige der geladenen Daten auf einer Karte (`MKMapView`) sowie in einer Tabelle.

## Verwendete Frameworks

### AutoLayoutKit

Vereinfachte Nutzung von Cocoa AutoLayout Constraints.
